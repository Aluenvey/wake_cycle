ruby -e '
  require "espeak"

  def goodbye
    farewell = "Farewell_"
    goodbye  =  "Goodbye_"
    see_you  =  "See_you_"

    goodbye_list = [
      farewell, goodbye, see_you
    ]

    $do_goodbye = goodbye_list.sample
  end

  def player
    player     = File.read("player/config_name.txt").strip

    $do_player = player
  end

  def nice_to_meet
    nice_seeing_you = "nice_seeing_you"
    good_seeing_you = "good_seeing_you"
    nice_to_see_you = "nice_to_see_you"

    someday_list = [
      nice_seeing_you, good_seeing_you, nice_to_see_you
    ]

    $do_nice_to_meet = someday_list.sample
  end

  g =      goodbye;
  p =       player;
  n = nice_to_meet;

  phrase = "#{$do_goodbye}#{$do_player}#{$do_nice_to_meet}."

  print_phrase = phrase.tr "_", " "

  speech = ESpeak::Speech.new(phrase)
  speech.speak

  puts "Bianca: #{print_phrase} "
'
