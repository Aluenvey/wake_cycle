# Create a greeting process method.
def greeting

  # Methods for determining which greeting statements.
  def if_hello
    your_name = File.read("farewell_player/player/config_name.txt").strip

    print          "Hello #{your_name}, "
  end

  def if_hi
    your_name = File.read("farewell_player/player/config_name.txt").strip

    print             "Hi #{your_name}, "
  end

  def if_heyo
    your_name = File.read("farewell_player/player/config_name.txt").strip

    print           "Heyo #{your_name}, "
  end

  def if_how_you_doing
    your_name = File.read("farewell_player/player/config_name.txt").strip

    print  "How you doing #{your_name}, "
  end

  def if_doing_well
    your_name = File.read("farewell_player/player/config_name.txt").strip

    print     "Doing well #{your_name}, "
  end

  # Create a greeting index.
  hello         = "Hello"
  hi            = "Hi"
  heyo          = "Heyo"
  hey           = "Hey"
  how_you_doing = "How_you_doing"
  doing_well    = "Doing_well"

  # Create a greeting conditional.
  if    $player_greeting ==         hello; i =         if_hello;
  elsif $player_greeting ==            hi; i =            if_hi;
  elsif $player_greeting ==          heyo; i =          if_heyo;
  elsif $player_greeting == how_you_doing; i = if_how_you_doing;
  elsif $player_greeting ==    doing_well; i =    if_doing_well;
  else
    print "I don't understand the greeting; "
  end
end

# Create an agent method.
def agent

  # Create an agent name.
  agent_name  = File.read("agent/agent_name.txt").strip

  # Conditional for agent name.
  if $agent_name == agent_name
    print "That's my name. "
  else
    print "That's not my name. "
  end
end

def do_algorithm

  # Do bayesian classification.
  def bayesian
    require "classifier-reborn"
    require "espeak"

    this_file = File.read("data/config_data.txt").strip

    speech = ESpeak::Speech.new("For_Bayesian_")
    speech.speak

    classifier = ClassifierReborn::Bayes.new 'Interesting', 'Uninteresting'

    classifier.train "Interesting", "Here are some good words. I hope you love them."
    classifier.train "Uninteresting", "Here are some bad words, I hate you."

    classified = classifier.classify this_file

    speech = ESpeak::Speech.new(classified)
    speech.speak
  end

  # Make decisions from a tree.
  def decision_tree
    require "decisiontree"
    require "espeak"

    speech = ESpeak::Speech.new("For_decision_tree_")
    speech.speak

    # If decision tree decides on basic commands
    def if_cal;     speech = ESpeak::Speech.new("calender."); speech.speak; system("cal");     end
    def if_date;    speech = ESpeak::Speech.new("date.");     speech.speak; system("date");    end
    def if_sensors; speech = ESpeak::Speech.new("sensors."); speech.speak; system("sensors"); end

    # if decision tree decides to create an assembler.
    def if_assembler
      speech = ESpeak::Speech.new("assembler."); speech.speak;

      # Write assembler file.
      open("assembled/hello.asm", "w") { |f|
        f.puts "
        global _start

           _start:

              msg db 'Hello World'
        "
      }

      # Compile and execute assembler.
      system("")
    end

    # Assign file input to input.
    input = File.read("input/config_input.txt").strip.to_i

    # Create the decision tree.
    attribute = ["Term"]

    training  = [
      [ 90.0,       "cal"], [180.0,      "date"],
      [270.0,   "sensors"], [360.0, "assembler"],
    ]

    dec_tree = DecisionTree::ID3Tree.new(attribute, training, "cal", :continuous)
    dec_tree.train

    test = [input]

    # Make prediction and actual test result.
    prediction = dec_tree.predict(test)
    actuality  = test.last

    # Conditional file test result.
    if    actuality ==  90.0; i =       if_cal;
    elsif actuality == 180.0; i =      if_date;
    elsif actuality == 270.0; i =   if_sensors;
    elsif actuality == 360.0; i = if_assembler;
    end
  end

  # Pick from a random sample.
  def gabbler_process
    require "gabbler"
    require "espeak"

  end

  # Create algorithmic choices.
  my_bayesian     =     "do_bayesian"
  my_decisiontree = "do_decisiontree"
  my_gabbler      =      "do_gabbler"

  # Conditional for picking an algorithm to do.
  if    $do_algorithm ==     my_bayesian; b =        bayesian;
  elsif $do_algorithm == my_decisiontree; d =   decision_tree;
  elsif $do_algorithm ==      my_gabbler; g = gabbler_process;
  else
    print "This algorithm is not recognized.\n"
  end
end

system("cd greet_player
        ./do_greet.sh")

print " >> "
input = gets.chomp

word_list = input.gsub(/\s+/m, " ").strip.split(" ")

$greeting    = word_list[0]
$agent       = word_list[1]
$algorithm   = word_list[2]

g =     greeting;
a =        agent;
d = do_algorithm;
